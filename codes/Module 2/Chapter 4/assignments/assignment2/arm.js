function arm(i) {
    var temp = i.toString();
    for (var j = 0; j < temp.length; j++) {
        var a = +(temp[j]);
        i -= Math.pow(a, 3);
    }
    return i == 0;
}
var p = document.getElementById("p1");
var str = "";
for (var i = 100; i <= 999; i++) {
    if (arm(i)) {
        str += i + " ";
    }
}
p.innerHTML = str;
//# sourceMappingURL=arm.js.map