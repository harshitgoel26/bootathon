function tab() {
    var a = document.getElementById("t1");
    var p = document.getElementById("p1");
    var q = document.getElementById("ta1");
    var b = +a.value;
    while (q.rows.length > 0) {
        q.deleteRow(1);
    }
    if (isNaN(b) || b < 1) {
        p.innerHTML = "Invalid";
    }
    else {
        p.innerHTML = "";
        for (var i = 1; i <= b; i++) {
            var r = q.insertRow();
            r.insertCell().innerHTML = b.toString();
            r.insertCell().innerHTML = "*";
            r.insertCell().innerHTML = i.toString();
            r.insertCell().innerHTML = "=";
            r.insertCell().innerHTML = (i * b).toString();
        }
    }
}
//# sourceMappingURL=table.js.map