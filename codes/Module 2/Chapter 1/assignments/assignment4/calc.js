function add() {
    var a = document.getElementById("t1");
    var b = document.getElementById("t2");
    var d = document.getElementById("p1");
    var c = +a.value + +b.value;
    d.value = c.toFixed(2);
}
function sub() {
    var a = document.getElementById("t1");
    var b = document.getElementById("t2");
    var d = document.getElementById("p1");
    var c = +a.value - +b.value;
    d.value = c.toFixed(2);
}
function mul() {
    var a = document.getElementById("t1");
    var b = document.getElementById("t2");
    var d = document.getElementById("p1");
    var c = +a.value * +b.value;
    d.value = c.toFixed(2);
}
function div() {
    var a = document.getElementById("t1");
    var b = document.getElementById("t2");
    var d = document.getElementById("p1");
    var c = +a.value / +b.value;
    d.value = c.toFixed(2);
}
function sin() {
    var a = document.getElementById("t1");
    var b = document.getElementById("r1");
    var c = document.getElementById("r2");
    var d = document.getElementById("p1");
    if (b.checked) {
        var x = Math.sin(+a.value);
        d.value = x.toFixed(2);
    }
    else {
        var x = Math.sin(+a.value * Math.PI / 180);
        d.value = x.toFixed(2);
    }
}
function cos() {
    var a = document.getElementById("t1");
    var b = document.getElementById("r1");
    var c = document.getElementById("r2");
    var d = document.getElementById("p1");
    if (b.checked) {
        var x = Math.cos(+a.value);
        d.value = x.toFixed(2);
    }
    else {
        var x = Math.cos(+a.value * Math.PI / 180);
        d.value = x.toFixed(2);
    }
}
function tan() {
    var a = document.getElementById("t1");
    var b = document.getElementById("r1");
    var c = document.getElementById("r2");
    var d = document.getElementById("p1");
    if (b.checked) {
        var x = Math.tan(+a.value);
        d.value = x.toFixed(2);
    }
    else {
        var x = Math.tan(+a.value * Math.PI / 180);
        d.value = x.toFixed(2);
    }
}
function sqr() {
    var a = document.getElementById("t1");
    var d = document.getElementById("p1");
    var c = Math.sqrt(+a.value);
    d.value = c.toFixed(2);
}
function pow() {
    var a = document.getElementById("t1");
    var b = document.getElementById("t2");
    var d = document.getElementById("p1");
    var c = Math.pow(+a.value, +b.value);
    d.value = c.toFixed(2);
}
//# sourceMappingURL=calc.js.map