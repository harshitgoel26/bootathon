function add(){
    var a:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var b:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    var d:HTMLInputElement=<HTMLInputElement>document.getElementById("p1");
    var c:number=+a.value + +b.value;
    d.value=c.toFixed(2);

}
function sub(){
    var a:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var b:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    var d:HTMLInputElement=<HTMLInputElement>document.getElementById("p1");
    var c:number=+a.value - +b.value;
    d.value=c.toFixed(2);

}
function mul(){
    var a:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var b:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    var d:HTMLInputElement=<HTMLInputElement>document.getElementById("p1");
    var c:number=+a.value * +b.value;
    d.value=c.toFixed(2);

}
function div(){
    var a:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var b:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    var d:HTMLInputElement=<HTMLInputElement>document.getElementById("p1");
    var c:number=+a.value / +b.value;
    d.value=c.toFixed(2);

}
function sin(){
    var a:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var b:HTMLInputElement=<HTMLInputElement>document.getElementById("r1");
    var c:HTMLInputElement=<HTMLInputElement>document.getElementById("r2");
    var d:HTMLInputElement=<HTMLInputElement>document.getElementById("p1");
    if(b.checked){
        var x:number=Math.sin(+a.value);
        d.value=x.toFixed(2);
    }
    else{
        var x:number=Math.sin(+a.value*Math.PI/180);
        d.value=x.toFixed(2);
    }
}
function cos(){
    var a:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var b:HTMLInputElement=<HTMLInputElement>document.getElementById("r1");
    var c:HTMLInputElement=<HTMLInputElement>document.getElementById("r2");
    var d:HTMLInputElement=<HTMLInputElement>document.getElementById("p1");
    if(b.checked){
        var x:number=Math.cos(+a.value);
        d.value=x.toFixed(2);
    }
    else{
        var x:number=Math.cos(+a.value*Math.PI/180);
        d.value=x.toFixed(2);
    }
}
function tan(){
    var a:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var b:HTMLInputElement=<HTMLInputElement>document.getElementById("r1");
    var c:HTMLInputElement=<HTMLInputElement>document.getElementById("r2");
    var d:HTMLInputElement=<HTMLInputElement>document.getElementById("p1");
    if(b.checked){
        var x:number=Math.tan(+a.value);
        d.value=x.toFixed(2);
    }
    else{
        var x:number=Math.tan(+a.value*Math.PI/180);
        d.value=x.toFixed(2);
    }
}
function sqr(){
    var a:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var d:HTMLInputElement=<HTMLInputElement>document.getElementById("p1");
    var c:number=Math.sqrt(+a.value);
    d.value=c.toFixed(2);
}
function pow(){
    var a:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var b:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    var d:HTMLInputElement=<HTMLInputElement>document.getElementById("p1");
    var c:number=Math.pow(+a.value,+b.value);
    d.value=c.toFixed(2);
}