function cosx() {
    var a = document.getElementById("t1");
    var b = document.getElementById("p1");
    var x = +a.value;
    if (isNaN(x)) {
        b.innerHTML = "Invalid number";
    }
    else {
        var c = x + Math.cos(x);
        b.innerHTML = "x+cos(x)is " + c;
    }
}
//# sourceMappingURL=cosx.js.map