function cosx(){
    var a:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var b:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("p1");
    
    var x:number=+a.value;
    if(isNaN(x)){
        b.innerHTML="Invalid number"
    }
    else{
        var c=x+Math.cos(x);
        b.innerHTML="x+cos(x)is "+c;
    }
}