function check(){
    var a:HTMLInputElement=<HTMLInputElement>document.getElementById("x1");
    var b:HTMLInputElement=<HTMLInputElement>document.getElementById("y1");
    var c:HTMLInputElement=<HTMLInputElement>document.getElementById("x2");
    var d:HTMLInputElement=<HTMLInputElement>document.getElementById("y2");
    var e:HTMLInputElement=<HTMLInputElement>document.getElementById("x3");
    var f:HTMLInputElement=<HTMLInputElement>document.getElementById("y3");
    var g:HTMLInputElement=<HTMLInputElement>document.getElementById("p1");
    var h:HTMLInputElement=<HTMLInputElement>document.getElementById("p2");
    var p:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("o1");
    
    var x1:number=+a.value;
    var y1:number=+b.value;
    var x2:number=+c.value;
    var y2:number=+d.value;
    var x3:number=+e.value;
    var y3:number=+f.value;
    var p1:number=+g.value;
    var p2:number=+h.value;

    var areatri:number=Math.abs(x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))/2;
    var area1:number=Math.abs(x2*(y3-p2)+x3*(p2-y2)+p1*(y2-y3))/2;
    var area2:number=Math.abs(x1*(y3-p2)+x3*(p1-y1)+p1*(y1-y3))/2;
    var area3:number=Math.abs(x1*(y2-p2)+x2*(p2-y1)+p1*(y1-y2))/2;
    if(Math.abs(areatri-(area1+area2+area3))<0.00001){
        p.innerHTML="The point lies inside triangle";
    }
    else{
        p.innerHTML="The point lies outside triangle";
    }

}