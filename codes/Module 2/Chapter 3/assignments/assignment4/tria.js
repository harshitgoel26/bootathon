function check() {
    var a = document.getElementById("x1");
    var b = document.getElementById("y1");
    var c = document.getElementById("x2");
    var d = document.getElementById("y2");
    var e = document.getElementById("x3");
    var f = document.getElementById("y3");
    var g = document.getElementById("p1");
    var h = document.getElementById("p2");
    var p = document.getElementById("o1");
    var x1 = +a.value;
    var y1 = +b.value;
    var x2 = +c.value;
    var y2 = +d.value;
    var x3 = +e.value;
    var y3 = +f.value;
    var p1 = +g.value;
    var p2 = +h.value;
    var areatri = Math.abs(x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2;
    var area1 = Math.abs(x2 * (y3 - p2) + x3 * (p2 - y2) + p1 * (y2 - y3)) / 2;
    var area2 = Math.abs(x1 * (y3 - p2) + x3 * (p1 - y1) + p1 * (y1 - y3)) / 2;
    var area3 = Math.abs(x1 * (y2 - p2) + x2 * (p2 - y1) + p1 * (y1 - y2)) / 2;
    if (Math.abs(areatri - (area1 + area2 + area3)) < 0.00001) {
        p.innerHTML = "The point lies inside triangle";
    }
    else {
        p.innerHTML = "The point lies outside triangle";
    }
}
//# sourceMappingURL=tria.js.map