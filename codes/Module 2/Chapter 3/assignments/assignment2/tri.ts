function tri(){
    var a:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    var b:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    var c:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");
    var p:HTMLParagraphElement=<HTMLParagraphElement>document.getElementById("p1");

    var x:number=+a.value;
    var y:number=+b.value;
    var z:number=+c.value;

    if((x==y)&&(y==z)){
        p.innerHTML="Equilateral";
    }
    else if((x==y)||(y==z)){
        p.innerHTML="Isosceles";
        if((Math.pow(x,2)==Math.pow(y,2)+Math.pow(z,2)) || (Math.pow(y,2)==Math.pow(x,2)+Math.pow(z,2))||(Math.pow(z,2)==Math.pow(y,2)+Math.pow(x,2))){
            p.innerHTML+="Right Angled";
        }

    }
    else{
        p.innerHTML="Scalene";
        if((Math.pow(x,2)==Math.pow(y,2)+Math.pow(z,2)) || (Math.pow(y,2)==Math.pow(x,2)+Math.pow(z,2))||(Math.pow(z,2)==Math.pow(y,2)+Math.pow(x,2))){
            p.innerHTML+="Right Angled";
        }
    }

}