function tri() {
    var a = document.getElementById("t1");
    var b = document.getElementById("t2");
    var c = document.getElementById("t3");
    var p = document.getElementById("p1");
    var x = +a.value;
    var y = +b.value;
    var z = +c.value;
    if ((x == y) && (y == z)) {
        p.innerHTML = "Equilateral";
    }
    else if ((x == y) || (y == z)) {
        p.innerHTML = "Isosceles";
        if ((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2))) {
            p.innerHTML += "Right Angled";
        }
    }
    else {
        p.innerHTML = "Scalene";
        if ((Math.pow(x, 2) == Math.pow(y, 2) + Math.pow(z, 2)) || (Math.pow(y, 2) == Math.pow(x, 2) + Math.pow(z, 2)) || (Math.pow(z, 2) == Math.pow(y, 2) + Math.pow(x, 2))) {
            p.innerHTML += "Right Angled";
        }
    }
}
//# sourceMappingURL=tri.js.map