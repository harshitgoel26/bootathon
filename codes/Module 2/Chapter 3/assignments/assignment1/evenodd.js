function eve() {
    var a = document.getElementById("t1");
    var b = document.getElementById("p1");
    if (+a.value == 0) {
        b.innerHTML = "Neither odd nor even";
    }
    else if ((+a.value % 2) == 0) {
        b.innerHTML = "The number is even";
    }
    else {
        b.innerHTML = "The number is odd";
    }
}
//# sourceMappingURL=evenodd.js.map